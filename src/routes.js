import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import HomePage from './views/HomePage/HomePage';
import EstatePage from './views/EstatePage/EstatePage';

const Routes = (props) => (
  <div className="bay-app">
    <Router>
      <Switch>
        <Route exact path='/' component={HomePage} />
        <Route exact path='/product/:id' component={EstatePage} />
      </Switch>
    </Router>
  </div>
);

export default Routes;