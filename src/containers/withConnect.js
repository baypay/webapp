import { connect } from 'react-redux';
const emptyFunc = () => ({});
export const withConnect = (mapStateToProps = emptyFunc, mapDispathToProps = emptyFunc) => (component) => {
  const finalMapStateToProps = (state, props) => {
      return {
          ...mapStateToProps(state, props),
      }
  }
  
  return connect(finalMapStateToProps)(component);
}