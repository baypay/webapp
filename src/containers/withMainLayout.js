import React from 'react';
import { Layout } from 'antd';

import MainMenu from '../components/MainMenu/MainMenu';

const { Header, Footer, Content } = Layout;

export const WithMainLayout = (WrappedComponent, menu = true) => (props) => (
  <Layout className="layout">
    { (menu) &&
      <Header className="header">
        <MainMenu />
      </Header>
    }
    <Content>
      <WrappedComponent />
    </Content>
    <Footer className="footer">
      <img 
        className="logo"
        title="Bayhouse" 
        alt="Bayhouse" 
        src="/assets/logo.svg" 
      />
    </Footer>
  </Layout>
);