import { combineReducers } from 'redux';
import { estates } from './estates';

const reducers = combineReducers({
  estates,
});

export default reducers;