import {
  GET_ESTATES
} from '../constants/estates';

const initialState = () => ({
  estates: [],
  loading: false,
  errors: []
});

export const estates = (state = initialState(), action) => {
  switch (action.type) {
    case GET_ESTATES.REQUEST:
     return { ...state, loading: true }
    case GET_ESTATES.SUCCESS:
     return { 
       ...state, 
       estates: action.estates,
       loading: false
      }
    case GET_ESTATES.FAILURE:
      return { 
        ...state,
        loading: false,
        errors: action.errors 
      }
    default:
      return state;
  }
}