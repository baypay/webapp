import React, {Component} from 'react';
import { Row, Col, Button } from 'antd';
import Swiper from 'react-id-swiper';
import { Navigation } from 'swiper/dist/js/swiper.esm';

import 'react-id-swiper/src/styles/css/swiper.css';

import { WithMainLayout } from '../../containers/withMainLayout';

import './EstatePage.scss';

const params = {
  slidesPerView: 1,
  spaceBetween: 0,
  navigation: {
    nextEl: '.swiper-button-next.navigation',
    prevEl: '.swiper-button-prev.navigation'
  },
  containerClass: 'slider'
};
class EstatePage extends Component {
  render(){
    return(
      <div className="EstatePage HomeBox">
        <div className="slideWrapper">
          <Swiper 
            modules={[Navigation]}
            {...params}
          >
            <div>
              <img 
                alt="Foto de la propiedad" 
                className="photo"
                src='https://source.unsplash.com/random/?house'
              />
            </div>
            <div><div className="mapLocation"/></div>
          </Swiper>
        </div>
        <div className="price">
          <h3 className="current">$350</h3>
          <h4 className="before">$460</h4>
        </div>
        <div className="info">
          <div className="roomers">14</div>
          <div className="isSecure">Seguro</div>
        </div>
      </div>
    );
  }
};

export default WithMainLayout(EstatePage, false);