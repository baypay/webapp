import React, {Component} from 'react';
import { Row, Col, Button } from 'antd';

import { WithMainLayout } from '../../containers/withMainLayout';

import Map from '../../components/Map/Map';
import SlideMode from '../../components/SlideMode/SlideMode';
import GridMode from '../../components/GridMode/GridMode';

import './HomePage.scss';

class HomePage extends Component {
  state = {
    mode: 1
  }
  switchMode = () => {
    this.setState({
      mode: !this.state.mode
    });
  }
  render(){
    const mode = this.state.mode;
    const switchModeClass = mode ? 'catalog' : 'slider';
    
    return(
      <div className="HomePage">
        <div className="mapSection">
          <Map/>
        </div>
        <div className="homeSection">
          <div className="preferences">
            <h3 className="title">Recomendados</h3>
            <Button className="btn primary" onClick={this.switchMode}>
              <i className={`icon ${switchModeClass}`}/>
            </Button>
          </div>
          <Row>
            {
              (mode) ? 
                <Col span={24}>
                  <SlideMode estates={25}/> 
                </Col>
              :
                <GridMode estates={20}/>
            }
          </Row>
        </div>
      </div>
    );
  }
};

export default WithMainLayout(HomePage);