import { takeEvery, call, put } from 'redux-saga/effects';
import { GET_ESTATES } from '../constants/estates';
import { getEstatesSuccess } from '../actions/estates'; 

import { getCall } from '../request';

function* getEstates(action){

  const { response, error } = yield call(getCall('estates'));
  yield put(getEstatesSuccess(response));
}

export function* estatesSagas(){
  yield takeEvery(GET_ESTATES.REQUEST, getEstates);
}