import { all } from 'redux-saga/effects';
import { estatesSagas } from './estates';

export default function* root(){
  yield all([
    estatesSagas(),
  ]);
}