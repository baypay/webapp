import {
  GET_ESTATES
} from '../constants/estates';

export const getEstates = () => ({
  type: GET_ESTATES.REQUEST
});

export const getEstatesSuccess = () => ({
  type: GET_ESTATES.REQUEST
});

export const getEstatesFailure = () => ({
  type: GET_ESTATES.REQUEST
});