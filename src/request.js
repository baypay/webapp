const baseURL = "https://bay-house.herokuapp.com/api/v1";

export const getCall = async (endpoint) => {
  const resp = await fetch(`${baseURL}${endpoint}`);
  const data = await resp.json();
  return data;
}