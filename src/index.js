import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { routerMiddleware, ConnectedRouter } from 'react-router-redux';
import { createBrowserHistory } from 'history';

import Routes from './routes';
import reducers from './reducers';
import sagas from './sagas';

import './styles/index.scss';
import "antd/dist/antd.css";

import { register } from './serviceWorker';

const history =  createBrowserHistory();
const historyRouterMiddleware =  routerMiddleware(history);

const sagaMiddleware = createSagaMiddleware();
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || ((fn) => fn);

const store = createStore(
  reducers,
  {},
  composeEnhancers(
    applyMiddleware(historyRouterMiddleware, sagaMiddleware)
  )
);

sagaMiddleware.run(sagas);

const App = (props) => (
  <Provider store={store}>
      <ConnectedRouter history={history}>
        <Routes />
      </ConnectedRouter>
  </Provider>
);

/* console.log('Provider', Provider);
console.log('ConnectedRouter', ConnectedRouter);
console.log('Route', Routes)
console.log('Register', register);
console.log('App', App); */

render(<App />, document.getElementById('root'));
register();
