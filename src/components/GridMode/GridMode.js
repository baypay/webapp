import React from 'react';
import { Col } from 'antd';
import HomeBox from '../HomeBox/HomeBox';

import './GridMode.scss';

const GridMode = (props) => {
  const { estates: qty } = props;
  const estates = Array.from(Array(qty), (x, index) => index + 1);

  return estates.map((estate) => (
    <Col key={estate} span={4} className="homeWrapper">
      <HomeBox/>
    </Col>
  ));
};

export default GridMode;