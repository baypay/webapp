import React, {Component} from 'react';
import { Map, GoogleApiWrapper } from 'google-maps-react';
import { apiKey } from '../../constants/map';
import classnames from 'classnames';
import { Input, Button, Tooltip } from 'antd';

import Rating from '../filters/StarRating/StarRating';

import './Map.scss';

const mapstyles = {
  position: 'relative',
  width: '100%',
  height: '100%',
  borderRadius: '10px'
};

class MapContainer extends Component{
  constructor(props){
    super(props);
    this.state = {
      mapOpen: false
    };
  }

  toggleMap = () => {
    this.setState({
      mapOpen: true
    });
  }
  render(){
    const mapOpenClass = (this.state.mapOpen) && 'open';
    return (
      <div className={classnames('MapSearch', mapOpenClass)}>
        <div 
          className='MapContainer'
          onClick={this.toggleMap}
        >
          <div className="overlay light"/>
          <Map
            className="map"
            google={this.props.google}
            zoom={14}
            style={mapstyles}
            initialCenter={{
              lat: 13.693041,
              lng: -89.2224143
            }}
            mapTypeControl={false}
            streetViewControl={false}
            fullscreenControl={false}
          />
          <div className="tile">
            <i className="icon marker"/>
            <strong>Usa el mapa</strong> para encontrar casas disponibles
          </div>
        </div>
        <div className="filters">
          <div className="filter starRating">
            <Rating/>
          </div>
          <div className="filter price">
            <div className="from">
              <Input size="small" placeholder="0.00"/>
            </div>
            <div className="to">
              <Input size="small" placeholder="0.00"/>
            </div>
          </div>
          <Tooltip 
            placement="left"
            title={<span>Crea tu <strong>casa ideal</strong></span>}
          >
            <Button className="btn primary filter dreamHome" onClick={this.switchMode}>
              <i className="icon dreamHome"/>
            </Button>
          </Tooltip>
        </div>
      </div>
    );
  }
}

export default GoogleApiWrapper({ 
  apiKey
})(MapContainer);