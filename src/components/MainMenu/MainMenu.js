import React from 'react';
import { Menu } from 'antd';

import './MainMenu.scss';

const { Item } = Menu;

const MainMenu = (props) => (
  <div className="mainMenu">
    <div className="photo" style={{ background: 'url(http://bit.ly/2PFOtW8)', backgroundSize: 'cover' }}/>
    <Menu
      className="menu"
      theme="light"
      mode="horizontal"
      defaultSelectedKeys={['2']}
    >
      <Item className="item login" key="1">Login</Item>
      <span>or</span>
      <Item className="item signup" key="2">Signup</Item>
    </Menu>
  </div>
);

export default MainMenu;