import React, {Component} from 'react';
import classnames from 'classnames';
import { sample } from 'lodash';

import { Tooltip } from 'antd';
 
import './HomeBox.scss';

const randomSize = (min, max) => (
  Math.floor(Math.random() * max) + min 
);

const secure = () => sample([true, false]);

class HomeBox extends Component {
  state = {
    selected: false,
    photo : `https://source.unsplash.com/random/${randomSize(200,350)}x${randomSize(150,250)}?house`,
    price: {
      current: randomSize(150,250),
      before: randomSize(300,500)
    },
    roomers: randomSize(1,20),
    isSecure: secure()
  }
  onBoxSelected= () => {
    this.setState({
      selected : !this.state.selected
    });
  }
  render(){
    const HomeBoxClasses =  this.state.selected ? classnames("HomeBox", 'selected') : 'HomeBox';
    return (
      <div 
        className={HomeBoxClasses} 
        onClick={this.onBoxSelected}
      >
        <div className="photo" style={{
          background: `url(${this.state.photo}) 50% no-repeat`,
          backgroundSize: 'cover'
        }}/>
        <div className="price">
          <h3 className="current">${this.state.price.current}</h3>
          <h4 className="before">${this.state.price.before}</h4>
        </div>
        { (this.state.isSecure) &&
          <Tooltip 
            placement="top"
            title={<span>Esta casa es <strong>ideal</strong></span>}
          >
            <div className="ideal"/>
          </Tooltip>
        }
        <div className="info">
          <div className="roomers">{this.state.roomers}</div>
          { (this.state.isSecure) &&
            <div className="isSecure">Seguro</div>
          }
        </div>
        <div className="openlink-anim"></div>
      </div>
    );
  }
}

export default HomeBox;