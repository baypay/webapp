import React from 'react';
import Swiper from 'react-id-swiper';
import HomeBox from '../HomeBox/HomeBox';
import { Navigation } from 'swiper/dist/js/swiper.esm';

import 'react-id-swiper/src/styles/css/swiper.css';

import './SlideMode.scss';

const SlideMode = (props) => {
  const params = {
    slidesPerView: 5,
    spaceBetween: 26,
    navigation: {
      nextEl: '.swiper-button-next.navigation',
      prevEl: '.swiper-button-prev.navigation'
    },
    containerClass: 'slide-mode'
  };

  const { estates: qty } = props;
  const estates = Array.from(Array(qty), (x, index) => index + 1);
 
  return (
    <div className="slideWrapper row-15">
      <div className="slide">
        <Swiper 
          modules={[Navigation]}
          {...params}
        >
          {
            estates.map((estate) => (
              <div key={estate}><HomeBox/></div>
            ))
          }
        </Swiper>
      </div>
    </div>
  );
};

export default SlideMode;